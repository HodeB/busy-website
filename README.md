**THIS PROJECT IS NO LONGER IN ACTIVE DEVELOPEMENT**

# BuSy project repository

This project aims to implement a fully working PoS
system integrated with a CRM and ASM system.

The stack used consists of MYSQL(database) -
Flux(state management) - ReactJs(front end) -
Node.JS(back-end) - Webpack(HTTP server)

# Current plans

* *PRIMARY:* Implement product register
* Look into Material UI
* Plan for a dashboard
