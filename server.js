var express  = require('express'),
    path     = require('path'),
    bodyParser = require('body-parser'),
    app = express(),
    expressValidator = require('express-validator');


/*Set EJS template Engine*/
app.set('views','./views');
app.set('view engine','ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true })); //support x-www-form-urlencoded
app.use(bodyParser.json());
app.use(expressValidator());

/*MySql connection*/
var connection  = require('express-myconnection'),
    mysql = require('mysql');

app.use(

    connection(mysql,{
        host     : 'localhost',
        user     : 'busyadmin',
        password : 'busy',
        database : 'busydata',
        debug    : true //set true if you wanna see debug logger
    },'request')

);

app.get('/',function(req,res){
    res.send('Welcome');
});


//RESTful route
var router = express.Router();


/*------------------------------------------------------
*  This is router middleware,invoked everytime
*  we hit url /api and anything after /api
*  like /api/user , /api/user/7
*  we can use this for doing validation,authetication
*  for every route started with /api
--------------------------------------------------------*/
router.use(function(req, res, next) {
    console.log(req.method, req.url);
    next();
});

var selectRoute = router.route('/select');


selectRoute.get(function(req,res,next){

    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query(req.query.query,function(err,results){

            if(err){
                console.log(err);
                return next("Mysql error, check your query");
            }

          res.json({resultater: results});

         });

    });

});

var insertPeopleRoute = router.route('/insertPeople');

insertPeopleRoute.get(function(req,res,next){

    var data = {
        fornavn:req.query.data.fornavn,
        etternavn:req.query.data.etternavn,
        tittel:req.query.data.tittel,
        epost:req.query.data.epost,
        tlfnr:req.query.data.tlfnr,
        firma:req.query.data.firma
     };

    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query(req.query.query, data, function(err){

            if(err){
                console.log(err);
                return next("Mysql error, check your query");
            }

          res.json({queryOk: 'true'});

         });

    });

});

var insertCompanyRoute = router.route('/insertCompany');

insertCompanyRoute.get(function(req,res,next){

    var data = {
        navn:req.query.data.navn,
        orgnr:req.query.data.orgnr,
        adr:req.query.data.adr,
        levadr:req.query.data.levadr,
        postnr:req.query.data.postnr,
        poststed:req.query.data.poststed,
        type:req.query.data.type
     };

    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query(req.query.query, data, function(err){

            if(err){
                console.log(err);
                return next("Mysql error, check your query");
            }

            res.json({queryOk: 'true'});

         });

    });

});

var updatePersonRoute = router.route('/people/:person_id');

/*------------------------------------------------------
route.all is extremely useful. you can use it to do
stuffs for specific routes. for example you need to do
a validation everytime route /api/user/:user_id it hit.

remove curut2.all() if you dont want it
------------------------------------------------------*/
updatePersonRoute.all(function(req,res,next){
    console.log("You need to smth about curut2 Route ? Do it here");
    console.log(req.params);
    next();
});

//get data to update
updatePersonRoute.get(function(req,res,next){

    var person_id = req.params.person_id;

    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("SELECT * FROM personer WHERE id = ? ",[person_id],function(err,rows){

            if(err){
                console.log(err);
                return next("Mysql error, check your query");
            }

            //if user not found
            if(rows.length < 1)
                return res.send("User Not found");

            res.render('edit',{title:"Edit user",data:rows});
        });

    });

});

//update data
updatePersonRoute.put(function(req,res,next){
    var person_id = req.params.person_id;

    //validation
    req.assert('name','Name is required').notEmpty();

    var errors = req.validationErrors();
    if(errors){
        res.status(422).json(errors);
        return;
    }

    //get data
    var data = {
        name:req.body.name
     };

    //inserting into mysql
    req.getConnection(function (err, conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("UPDATE personer set ? WHERE person_id = ? ",[data,person_id], function(err, rows){

           if(err){
                console.log(err);
                return next("Mysql error, check your query");
           }

          res.sendStatus(200);

        });

     });

});

//delete data
updatePersonRoute.delete(function(req,res,next){

    var person_id = req.params.person_id;

     req.getConnection(function (err, conn) {

        if (err) return next("Cannot Connect");

        var query = conn.query("DELETE FROM person  WHERE person_id = ? ",[person_id], function(err, rows){

             if(err){
                console.log(err);
                return next("Mysql error, check your query");
             }

             res.sendStatus(200);

        });
        //console.log(query.sql);

     });
});

var insertProductsRoute = router.route('/insertProducts');

insertProductsRoute.get(function(req,res,next){

    var data = {
      name:req.query.data.name,
      provider:req.query.data.provider,
      prodnr:req.query.data.prodnr,
      amount:req.query.data.amount,
      pricebuy:req.query.data.pricebuy,
      pricesale:req.query.data.pricesale,
      text:req.query.data.text
     };

    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query(req.query.query, data, function(err){

            if(err){
                console.log(err);
                return next("Mysql error, check your query");
            }

          res.json({queryOk: 'true'});

         });

    });

});

var updateProductRoute = router.route('/products/:product_id');

//update data
updateProductRoute.put(function(req,res,next){
    var product_id = req.params.product_id;

    //validation
    req.assert('name','Name is required').notEmpty();

    var errors = req.validationErrors();
    if(errors){
        res.status(422).json(errors);
        return;
    }

    //get data
    var data = {
        name:req.body.name
     };

    //inserting into mysql
    req.getConnection(function (err, conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("UPDATE products set ? WHERE id = ? ",[data,product_id], function(err, rows){

           if(err){
                console.log(err);
                return next("Mysql error, check your query");
           }

          res.sendStatus(200);

        });

     });

});

updateProductRoute.get(function(req,res,next){

    var product_id = req.params.product_id;

    req.getConnection(function(err,conn){

        if (err) return next("Cannot Connect");

        var query = conn.query("SELECT * FROM products WHERE id = ? ",[product_id],function(err,rows){

            if(err){
                console.log(err);
                return next("Mysql error, check your query");
            }

            if(rows.length < 1)
                return res.send("Product Not found");

            res.render('edit',{title:"Edit user",data:rows});
        });

    });

});

//delete data
updateProductRoute.delete(function(req,res,next){

    var product_id = req.params.product_id;

     req.getConnection(function (err, conn) {

        if (err) return next("Cannot Connect");

        var query = conn.query("DELETE FROM products  WHERE person_id = ? ",[product_id], function(err, rows){

             if(err){
                console.log(err);
                return next("Mysql error, check your query");
             }

             res.sendStatus(200);

        });
        //console.log(query.sql);

     });
});

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

//now we need to apply our router here
app.use('/api', router);

//start Server
var server = app.listen(3001,function(){

   console.log("Listening to port %s",server.address().port);

});
