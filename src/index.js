import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, } from 'react-router-dom';

import Layout from './pages/Layout.js';
import Kontakter from './pages/kontaktmodul/Kontakter.js';
import Produkter from './pages/produktregister/Produkter.js';

ReactDOM.render(
  <Router>
    <div>
      <Route path='/' component={Layout}></Route>
      <Route path='/Kontakter' component={Kontakter}></Route>
      <Route path='/Produkter' component={Produkter}></Route>
    </div>
  </Router>,
  document.getElementById('root')
);
