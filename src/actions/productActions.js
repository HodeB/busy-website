import dispatcher from '../dispatcher';
import request from 'superagent';

export function deleteProduct(id) {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/select')
      .query({query: 'DELETE FROM products WHERE id = ' + id})
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          resolve(res.body.resultater);
        }
      });
  });

  queryDB.then(function successHandler() {
    reloadProducts();
  });
}
export function addProduct(data) {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/insertProducts')
      .query({query: 'INSERT INTO products SET ? ',
              data : data})
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          resolve(res.text);
        }
      });
  });

  queryDB.then(function successHandler() {
    reloadProducts();
  });
}
export function updateProduct(id, data) {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/insertProducts')
      .query({query: 'UPDATE products SET ? WHERE id = ' + id,
              data : data})
      .set('Accept', 'application/json')
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          resolve(res.text);
        }
      });
  });

  queryDB.then(function successHandler() {
    reloadProducts();
  });
}
export function reloadProducts() {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/select')
      .query({query: 'SELECT * FROM products'})
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          resolve(res.body.resultater);
        }
      });
  });

  queryDB.then(function successHandler(result) {
    dispatcher.dispatch({
      type: "RECEIVE_PRODUCTS",
      products: result
      });
  });
}
export function updateSearch(search) {
  dispatcher.dispatch({
    type: "UPDATE_SEARCH",
    search: search
  });
}
export function updateSearchTerms(searchterms) {
  dispatcher.dispatch({
    type: "UPDATE_SEARCHTERMS",
    searchterms: searchterms
  });
}
export function editProduct(id) {
  dispatcher.dispatch({
    type: "UPDATE_INPUT",
    id: id
  });
}
export function clearActiveProduct() {
  dispatcher.dispatch({
    type: "CLEAR_INPUT"
  });
}
export function getProductById(id) {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/select')
      .query({query: 'SELECT * FROM products WHERE id = ' + id})
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          resolve(res.body.resultater);
        }
      });
  });

  queryDB.then(function successHandler(result) {
    dispatcher.dispatch({
      type: "RECEIVE_PRODUCT_BYID",
      product: result
      });
  });
}
