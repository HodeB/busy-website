import dispatcher from '../dispatcher';
import request from 'superagent';

export function deletePerson(id) {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/select')
      .query({query: 'DELETE FROM personer WHERE id = ' + id})
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          resolve(res.body.resultater);
        }
      });
  });

  queryDB.then(function successHandler() {
    reloadPeople();
  });
}
export function addPerson(data) {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/insertPeople')
      .query({query: 'INSERT INTO personer SET ? ',
              data : data})
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          resolve(res.text);
        }
      });
  });

  queryDB.then(function successHandler() {
    reloadPeople();
  });
}
export function updatePerson(id, data) {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/insertPeople')
      .query({query: 'UPDATE personer SET ? WHERE id = ' + id,
              data : data})
      .set('Accept', 'application/json')
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          resolve(res.text);
        }
      });
  });

  queryDB.then(function successHandler() {
    reloadPeople();
  });
}
export function reloadPeople() {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/select')
      .query({query: 'SELECT * FROM personer'})
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          resolve(res.body.resultater);
        }
      });
  });

  queryDB.then(function successHandler(result) {
    dispatcher.dispatch({
      type: "RECEIVE_PEOPLE",
      people: result
      });
  });
}
export function updateSearch(search) {
  dispatcher.dispatch({
    type: "UPDATE_SEARCH",
    search: search
  });
}
export function updateSearchTerms(searchterms) {
  dispatcher.dispatch({
    type: "UPDATE_SEARCHTERMS",
    searchterms: searchterms
  });
}
export function editPerson(id) {
  dispatcher.dispatch({
    type: "UPDATE_INPUT",
    id: id
  });
}
export function clearActivePerson() {
  dispatcher.dispatch({
    type: "CLEAR_INPUT"
  });
}
export function getPersonById(id) {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/select')
      .query({query: 'SELECT * FROM personer WHERE id = ' + id})
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          resolve(res.body.resultater);
        }
      });
  });

  queryDB.then(function successHandler(result) {
    dispatcher.dispatch({
      type: "RECEIVE_PERSON_BYID",
      person: result
      });
  });
}
