import dispatcher from '../dispatcher';
import request from 'superagent';

export function reloadCompanies() {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/select')
      .query({query: 'SELECT * FROM firmaer'})
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          resolve(res.body.resultater);
        }
      });
  });

  queryDB.then(function successHandler(result) {
    dispatcher.dispatch({
      type: "RECEIVE_COMP",
      firmaer: result
      });
  });
}
export function deleteCompany(id) {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/select')
      .query({query: 'DELETE FROM firmaer WHERE id = ' + id})
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          resolve(res.body.resultater);
        }
      });
  });

  queryDB.then(function successHandler() {
    reloadCompanies();
  });
}

export function addCompany(data) {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/insertCompany')
      .query({query: 'INSERT INTO firmaer SET ? ',
              data : data})
      .set('Accept', 'application/json')
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          resolve(res.text);
        }
      });
  });

  queryDB.then(function successHandler() {
    reloadCompanies();
  });
}

export function updateCompany(id, data) {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/insertCompany')
      .query({query: 'UPDATE firmaer SET ? WHERE id = ' + id,
              data : data})
      .set('Accept', 'application/json')
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          console.log(res.text);
        }
      });
  });

  queryDB.then(function successHandler(result) {
    reloadCompanies();
  });
}

export function updateSearch(text) {
  dispatcher.dispatch({
    type: "UPDATE_SEARCH",
    text: text
  });
}
export function updateSearchTerms(text) {
  dispatcher.dispatch({
    type: "UPDATE_SEARCHTERMS",
    text: text
  });
}
export function editCompany(id) {
  dispatcher.dispatch({
    type: "UPDATE_INPUT",
    id: id
  });
}
export function clearActiveCompany() {
  dispatcher.dispatch({
    type: "CLEAR_INPUT"
  });
}
export function getCompanyById(id) {
  const queryDB = new Promise( function( resolve, reject ) {
    request
      .get('http://localhost:3001/api/select')
      .query({query: 'SELECT * FROM firmaer WHERE id = ' + id})
      .end(function(err, res){
        if (err || !res.ok) {
          alert('Database error. Please tell Martin');
        } else {
          resolve(res.body.resultater);
        }
      });
  });

  queryDB.then(function successHandler(result) {
    dispatcher.dispatch({
      type: "RECEIVE_COMP_BYID",
      firma: result
      });
  });
}
