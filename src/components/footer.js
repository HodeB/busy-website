import React from 'react';

function Footer() {
  return (
    <div className="Footer">
      <div>
        <p>
          <img 
            style={{width: 30, height: 30}}
            src={require('../images/copyleft.png')}
            alt=""
          />
          Copyleft Martin Blondin 2016
        </p>
      </div>
    </div>
  );
}

export default Footer;
