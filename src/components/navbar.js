import React, { Component } from 'react';
import { Navbar, Nav, NavItem } from 'react-bootstrap';

function Logo() {
  return (
    <img 
      style={{width: 60}}
      src={require('../images/logo.svg')}
      alt=""
    />
  );
}
class NavbarInstance extends Component {
  handeClick(e){
    const path = e.target.id;
    this.props.navigate(path);
  }
  render() {
    return(
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <a id='' onClick={this.handeClick.bind(this)}>
              <Logo />
            </a>
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          <NavItem id='Kontakter' onClick={this.handeClick.bind(this)} eventKey={2} >Kontakter</NavItem>
          <NavItem id='Produkter' onClick={this.handeClick.bind(this)} eventKey={2} >Produkter</NavItem>
        </Nav>
      </Navbar>
    );
  }
}

export default NavbarInstance;
