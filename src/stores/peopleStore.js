import { EventEmitter } from "events";
import dispatcher from "../dispatcher.js";

class PeopleStore extends EventEmitter {
  constructor(){
    super();
    this.people = [
    {
      fornavn: 'Vennligst vent...',
      etternavn: '',
      tittel: '',
      epost: '',
      tlfnr: '',
      firma: ''
    }
    ];
    this.search = "";
    this.searchterms = "Navn";
    this.activeEdit = '';
    this.activePerson = {};
    this.setMaxListeners(30);
  }
  receivePeople(people){
    this.people = people;
    this.emit("Change");
  }
  receivePerson(person){
    this.activePerson = person;
    this.emit("ChangeActive");
  }
  updateSearch(search){
    this.search = search;
    this.emit("ChangeSearch");
  }
  updateSearchTerms(searchterms){
    this.searchterms = searchterms;
    this.emit("ChangeSearch");
  }
  getPeople(){
    return this.people;
  }
  getSearch(){
    if (this.search){
      return this.search;
    }
    return '';
  }
  getSearchTerms(){
    if (this.searchterms){
      return this.searchterms;
    }
    return 'Navn';
  }
  updateInput(id){
    if(id){
      this.activeEdit = id;
      this.emit("ChangeEdit");
    }
  }
  clearInput(){
    this.activeEdit = '';
    this.activePerson = '';
    this.emit("ChangeEdit");
  }
  getActiveEdit(){
    return this.activeEdit;
  }
  getActivePerson(){
    return this.activePerson;
  }
  handleActions(action){
    if( action.type === "RECEIVE_PEOPLE"){
      this.receivePeople(action.people);
    }
    else if( action.type === "UPDATE_SEARCH"){
      this.updateSearch(action.search);
    }
    else if( action.type === "UPDATE_SEARCHTERMS"){
      this.updateSearchTerms(action.searchterms);
    }
    else if( action.type === "CLEAR_INPUT") {
      this.clearInput();
    }
    else if( action.type === "UPDATE_INPUT") {
      this.updateInput(action.id);
    }
    else if( action.type === "RECEIVE_PERSON_BYID") {
      this.receivePerson(action.person);
    }
  }
}

const peopleStore = new PeopleStore;
dispatcher.register(peopleStore.handleActions.bind(peopleStore));

window.dispatcher = dispatcher;

export default peopleStore;
