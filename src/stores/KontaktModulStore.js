import { EventEmitter } from "events";
import dispatcher from "../dispatcher.js"

class PeopleStore extends EventEmitter {
  constructor(){
    super();
    this.people = [
    {
      fornavn: 'Vennligst vent...',
      etternavn: '',
      tittel: '',
      epost: '',
      tlfnr: '',
      firma: ''
    }
    ];
  }
  receivePeople(people){
    const id = Date.now();
    this.people = people;
    this.emit("Change");
  }
  getAll(){
    return this.people;
  }
  handleActions(action){
    switch(action.type){
    case "RECEIVE_PEOPLE":{
      this.receivePeople(action.people);
    }
    case "RECEIVE_COMP":{
      this.receiveCompany(action.people);
    }
    }
  }
}

const peopleStore = new PeopleStore;
dispatcher.register(peopleStore.handleActions.bind(peopleStore));

window.dispatcher = dispatcher;

export default peopleStore;
