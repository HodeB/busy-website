import { EventEmitter } from "events";
import dispatcher from "../dispatcher.js";

class ProductStore extends EventEmitter {
  constructor(){
    super();
    this.products = [
    {
      name: 'Vennligst vent...',
      provider: '',
      prodnr: '',
      amount: '',
      pricebuy: '',
      pricesale: '',
      text:''
    }
    ];
    this.search = "";
    this.searchterms = "Navn";
    this.activeEdit = '';
    this.activeProduct = {};
    this.setMaxListeners(30);
  }
  receiveProducts(products){
    this.products = products;
    this.emit("Change");
  }
  receiveProduct(product){
    this.activeProduct = product;
    this.emit("ChangeActive");
  }
  updateSearch(search){
    this.search = search;
    this.emit("ChangeSearch");
  }
  updateSearchTerms(searchterms){
    this.searchterms = searchterms;
    this.emit("ChangeSearch");
  }
  getProducts(){
    return this.products;
  }
  getSearch(){
    if (this.search){
      return this.search;
    }
    return '';
  }
  getSearchTerms(){
    if (this.searchterms){
      return this.searchterms;
    }
    return 'Navn';
  }
  updateInput(id){
    if(id){
      this.activeEdit = id;
      this.emit("ChangeEdit");
    }
  }
  clearInput(){
    this.activeEdit = '';
    this.activeProduct = '';
    this.emit("ChangeEdit");
  }
  getActiveEdit(){
    return this.activeEdit;
  }
  getActiveProduct(){
    return this.activeProduct;
  }
  handleActions(action){
    if( action.type === "RECEIVE_PRODUCTS"){
      this.receiveProducts(action.products);
    }
    else if( action.type === "UPDATE_SEARCH"){
      this.updateSearch(action.search);
    }
    else if( action.type === "UPDATE_SEARCHTERMS"){
      this.updateSearchTerms(action.searchterms);
    }
    else if( action.type === "CLEAR_INPUT") {
      this.clearInput();
    }
    else if( action.type === "UPDATE_INPUT") {
      this.updateInput(action.id);
    }
    else if( action.type === "RECEIVE_PRODUCT_BYID") {
      this.receiveProduct(action.product);
    }
  }
}

const productStore = new ProductStore;
dispatcher.register(productStore.handleActions.bind(productStore));

window.dispatcher = dispatcher;

export default productStore;
