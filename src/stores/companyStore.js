import { EventEmitter } from "events";
import dispatcher from "../dispatcher.js";

class CompanyStore extends EventEmitter {
  constructor(){
    super();
    this.firmaer = [
    {
      navn: 'Vennligst vent...',
      orgnr: '',
      adr: '',
      levadr: '',
      postnr: '',
      poststed: '',
      type: ''
    }
    ];
    this.search = '';
    this.searchterms = '';
    this.activeEdit = '';
    this.activeCompany = {};
    this.setMaxListeners(30);
  }
  receiveCompanies(firmaer){
    const id = Date.now();
    this.firmaer = firmaer;
    this.emit("Change");
  }
  receiveCompany(firma){
    const id = Date.now();
    this.activeCompany = firma;
    this.emit("ChangeActive");
  }
  getAll(){
    return this.firmaer;
  }
  updateSearch(text){
    const id = Date.now();
    this.search = text;
    this.emit("ChangeSearch");
  }
  updateSearchTerms(text){
    const id = Date.now();
    this.searchterms = text;
    this.emit("ChangeSearch");
  }
  getSearch(){
    if (this.search){
      return this.search;
    }
    return '';
  }
  getSearchTerms(){
    if (this.searchterms){
      return this.searchterms;
    }
    return 'Navn';
  }
  updateInput(id){
    if(id){
      this.activeEdit = id;
      this.emit("ChangeEdit");
    }
  }
  clearInput(){
    this.activeEdit = '';
    this.activeCompany = {};
    this.emit("ChangeEdit");
  }
  getActiveEdit(){
    return this.activeEdit;
  }
  getActiveCompany(){
    return this.activeCompany;
  }
  handleActions(action){
    if(action.type === "UPDATE_SEARCHTERMS"){
      this.updateSearchTerms(action.text);
      
    }
    if( action.type === "RECEIVE_COMP" ){
      this.receiveCompanies(action.firmaer);
    }
    if( action.type === "UPDATE_SEARCH" ){
      this.updateSearch(action.text);
    }
    if( action.type === "CLEAR_INPUT" ){
      this.clearInput();
    }
    if( action.type === "UPDATE_INPUT" ){
      this.updateInput(action.id);
    }
    if( action.type === "RECEIVE_COMP_BYID" ){
      this.receiveCompany(action.firma);
    }
  }
}

const companyStore = new CompanyStore;
dispatcher.register(companyStore.handleActions.bind(companyStore));

window.dispatcher = dispatcher;

export default companyStore;
