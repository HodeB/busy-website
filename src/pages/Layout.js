import React, { Component } from 'react';
import '../css/Layout.css';
import NavbarInstance from '../components/navbar.js';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';

class Layout extends Component {
  navigate(path){
    this.props.history.push(path);
  }
  render() {
    return (
      <div>
        <NavbarInstance navigate={this.navigate.bind(this)}/>
      </div>
    );
  }
}

export default Layout;
