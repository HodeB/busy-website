import React, { Component } from 'react';
import * as productActions from '../../actions/productActions.js';
import productStore from '../../stores/productsStore.js';
import companyStore from '../../stores/companyStore.js';
import * as companyActions from '../../actions/companyActions.js';

class ProductForm extends Component {
  render() {
    return(
      <div id='productform' className="panel-group">
        <div className="panel panel-default" id="list">
          <div className="panel-body">
            <a id='mainproductimage'href="#" className="thumbnail">
              <img src={require('../../images/defaultimage.svg')}
                   />
            </a>
            <ProductFormField />
          </div>
        </div>
      </div>
    );
  }
}
class ProductFormField extends Component {
  constructor(){
    super();
    this.state ={
      firmaer: companyStore.getAll(),
      name: '',
      provider: '',
      amount: '',
      pricebuy: '',
      pricesale: '',
      text: '',
      editing: false,
      product: {},
      activeEdit: ''
    };
    this.listeners = {
      productChangeEdit: null,
      productChangeActive: null,
      companyChange: null
    };
  }
  componentWillMount(){
    this.reloadCompanies();
    this.listeners.productChangeEdit = this.getActiveEdit.bind(this);
    this.listeners.productChangeActive = this.getProduct.bind(this);
    this.listeners.companyChange = this.getCompanies.bind(this);
    productStore.on("ChangeEdit", this.listeners.productChangeEdit);
    productStore.on("ChangeActive", this.listeners.productChangeActive);
    productStore.on("Change", this.listeners.companyChange);
  }
  componentWillUnmount() {
    productStore.removeListener("ChangeEdit", this.listeners.productChangeEdit);
    productStore.removeListener("ChangeActive", this.listeners.productChangeActive);
    companyStore.removeListener("Change", this.listeners.companyChange);
  }
  reloadCompanies(){
    companyActions.reloadCompanies();
  }
  getCompanies(){
    this.setState({
      firmaer: companyStore.getAll()
    });
  }
  addProduct(data){
   productActions.addProduct(data);
  }
  updateProduct(id, data){
    productActions.updateProduct(id, data);
  }
  getActiveEdit(){
    if (productStore.getActiveEdit()){
      this.setState({activeEdit: productStore.getActiveEdit()}, function () {
        productActions.getProductById(this.state.activeEdit);
      });
    }else{
      this.clearActive();
    }
  }
  getProduct(){
    this.clearActive();
    this.setState({product: productStore.getActiveProduct()[0]}, function () {
      this.setState({
        name: this.state.product.name,
        provider: this.state.product.provider,
        prodnr: this.state.product.prodnr,
        amount: this.state.product.amount,
        pricebuy: this.state.product.pricebuy,
        pricesale: this.state.product.pricesale,
        text: this.state.product.text,
        editing: true,
        activeEdit: this.state.product.id
      });
    });
  }
  clearActive(){
    this.setState({
      name: '',
      provider: '',
      prodnr: '',
      amount: '',
      pricebuy: '',
      pricesale: '',
      text: '',
      editing: false,
      product: {},
      activeEdit: ''
    });
  }
  verifyData(data){
    if(data.name && data.provider && data.amount && data.pricebuy && data.pricesale && data.text){
      return true;
    }
    return false;
  }
  handleSubmit(e) {
    e.preventDefault();
    const formData = {};
    for (const field in this.refs) {
      formData[field] = this.refs[field].value;
    }
    if(this.verifyData(formData)){
      if (!this.state.editing){
        this.addProduct(formData);
      }else {
        this.updateProduct(this.state.activeEdit, formData);
      }
      this.clearActive();
    }else{
      alert('Vennligst fyll ut alle felt');
    }
  }
  testNumber(nr){
    var regexnr = new RegExp('^[0-9]*$');
    if(regexnr.test(nr)){
      return true;
    }
    return false;
  }
  nameHandleChange(e){
    this.setState({name: e.target.value});
  }
  providerHandleChange(e){
    this.setState({provider: e.target.value});
  }
  prodnrHandleChange(e){
    this.setState({prodnr: e.target.value});
  }
  amountHandleChange(e){
    if(!this.testNumber(e.target.value)){
      this.setState({
        amountClassName: 'input-group has-error'
      });
      if(this.state.amountTimer){
        clearTimeout(this.state.amountTimer);
      }
      var timer = setTimeout(() => {this.setState({amountClassName: 'input-group'});}, 750);
      this.setState({amountTimer: timer});
    }else{
      this.setState({
        amountClassName: 'input-group',
        amount: e.target.value
      });
    }
  }
  pricebuyHandleChange(e){
    if(!this.testNumber(e.target.value)){
      this.setState({
        pricebuyClassName: 'input-group has-error'
      });
      if(this.state.pricebuyTimer){
        clearTimeout(this.state.pricebuyTimer);
      }
      var timer = setTimeout(() => {this.setState({pricebuyClassName: 'input-group'});}, 750);
      this.setState({pricebuyTimer: timer});
    }else{
      this.setState({
        pricebuyClassName: 'input-group',
        pricebuy: e.target.value
      });
    }
  }
  pricesaleHandleChange(e){
    if(!this.testNumber(e.target.value)){
      this.setState({
        pricesaleClassName: 'input-group has-error'
      });
      if(this.state.pricesaleTimer){
        clearTimeout(this.state.pricesaleTimer);
      }
      var timer = setTimeout(() => {this.setState({pricesaleClassName: 'input-group'});}, 750);
      this.setState({pricesaleTimer: timer});
    }else{
      this.setState({
        pricesaleClassName: 'input-group',
        pricesale: e.target.value
      });
    }
  }
  textHandleChange(e){
    this.setState({text: e.target.value});
  }
  render() {
    return(
      <div id='productformfield'>
        <form onSubmit={this.handleSubmit.bind(this)} className="input-group">
          <div className="row">
            <div className="col-md-4" style={{
                   width: '40%'
                 }}>
              <center>
              <label >Navn</label>
              <input type="text" className="form-control"
                     style={{
                     }}
                     value={this.state.name}
                     onChange={this.nameHandleChange.bind(this)}
                     ref='name'/>
              </center>
            </div>
            <div className="col-md-4" style={{
                   width: '40%'
                 }}>
              <center>
              <label >Leverandør</label>
              <select className="form-control" 
                      placeholder='Ingen firma'
                      value={this.state.provider}
                      onChange={this.providerHandleChange.bind(this)}
                      ref='provider'>
                {this.state.firmaer.map(function(object){
                  return (
                    <option value={object.id}>{object.navn}</option>
                  );
                })}
              </select>
              </center>
            </div>
          </div>
          <div className="row" style={{'marginTop': '6%'}}>
            <div className="col-md-4" style={{
                   width: '40%'
                 }}>
              <center>
              <label >Produknr</label>
              <input type="text" className="form-control"
                     style={{
                     }}
                      value={this.state.prodnr}
                      onChange={this.prodnrHandleChange.bind(this)}
                     ref='prodnr'/>
              </center>
            </div>
            <div className="col-md-4" style={{
                   width: '40%'
                 }}>
              <center>
              <label >Antall</label>
              <div className={this.state.amountClassName}>
              <input type="text" className="form-control"
                     style={{
                     }}
                      value={this.state.amount}
                      onChange={this.amountHandleChange.bind(this)}
                     ref='amount'/>
              </div>
              </center>
            </div>
          </div>
          <div className="row" style={{'marginTop': '6%'}}>
            <div className="col-md-4" style={{
                   width: '40%'
                 }}>
              <center>
              <label >Pris kjøp <span className="badge">NOK</span></label>
              <div className={this.state.pricebuyClassName}>
              <input type="text" className="form-control"
                     style={{
                     }}
                      value={this.state.pricebuy}
                      onChange={this.pricebuyHandleChange.bind(this)}
                     ref='pricebuy'/>
              </div>
              </center>
            </div>
            <div className="col-md-4" style={{
                   width: '40%'
                 }}>
              <center>
              <label >Pris salg <span className="badge">NOK</span></label>
              <div className={this.state.pricesaleClassName}>
              <input type="text" className="form-control"
                     style={{
                     }}
                     value={this.state.pricesale}
                     onChange={this.pricesaleHandleChange.bind(this)}
                     ref='pricesale'/>
              </div>
              </center>
            </div>
            <div style={{
              width: '40%'
            }}>
              <center>
        <label style={{marginTop: '7%'}}>Deskripsjon</label>
              <textarea className="form-control"
                     style={{
                          resize: 'none'
                     }}
                     value={this.state.text}
                     onChange={this.textHandleChange.bind(this)}
                     ref='text'/>
              </center>
            </div>
          </div>
          <button style={{
                    marginBottom: '2%', marginTop: '7%', float: 'right'
                  }}type='submit' className="btn btn-success">
            <span className='glyphicon glyphicon-plus'></span>
          </button>
        </form>
      </div>
    );
  }
}
export default ProductForm;
