import React, { Component } from 'react';
import * as productActions from '../../actions/productActions.js';
import productStore from '../../stores/productsStore.js';
import * as companyActions from '../../actions/companyActions.js';
import request from 'superagent';

class ProductOverview extends Component {
  constructor(){
    super();
    this.state = {
      products: productStore.getProducts(),
      search: productStore.getSearch(),
      searchterms: 'Navn',
      page: 0,
      maxPages: 0
    };
    this.listeners = {
      productChange: null,
      productChangeSearch: null
    };
  }
  componentWillMount(){
    this.reloadProducts();
    this.listeners.productChange = this.getProducts.bind(this);
    this.listeners.productChangeSearch = this.getSearch.bind(this);
    productStore.on("Change", this.listeners.productChange);
    productStore.on("ChangeSearch", this.listeners.productChangeSearch);
  }
  componentWillUnmount() {
    productStore.removeListener("Change", this.listeners.productChange);
    productStore.removeListener("ChangeSearch", this.listeners.productChangeSearch);
  }
  reloadProducts(){
    productActions.reloadProducts();
  }
  getProducts(){
    this.setState({
      products: productStore.getProducts()
    }, function () {
      this.updateMaxPages(this.state.products);
    });
  }
  getSearch(){
    this.setState({
      search: productStore.getSearch(),
      searchterms: productStore.getSearchTerms(),
      page: 0
    }, function () {
      this.updateMaxPages(this.searchProducts(this.state.products));
      productActions.clearActiveProduct();
    });
  }
  searchProducts(data){
    var newData = [];
    var search = this.state.search;
    var searchterms = this.state.searchterms;
    if (!search){
      search = '';
    }
    data.map(function(object, i){
      var regex = '[\s\S]*' + search + '[\s\S]*';
      var regex = new RegExp(regex, 'i');
      if(searchterms == 'Navn'){
        if (regex.test(object.name)){
          newData.push(object);
        }
      }
      if(searchterms == 'Produktnr'){
        if (regex.exec(object.prodnr)){
          newData.push(object);
        }
      }
      if(searchterms == 'Leverandør'){
        if (regex.exec(object.provider)){
          newData.push(object);
        }
      }
    });
    return newData;
  }
  returnIndexes(array, startIndex, endIndex){
    var newArray = [];
    var i = startIndex;
    while(i < array.length){
      newArray.push(array[i]);
      i += 1;
      if(i > endIndex){
        break;
      }
    }
    return newArray;
  }
  returnPage(data){
    var page = this.state.page;
    return this.returnIndexes(data, page*10, (page+1)*10);
  }
  pageForwards(){
    if(this.state.page !== this.state.maxPages){
      this.setState({page: this.state.page + 1});
    }else{
      this.setState({page: 0});
    }
    if(productStore.getActiveEdit()){
      productActions.clearActiveProduct();
    }
  }
  pageBackwards(){
    if(this.state.page > 0){
      this.setState({page: this.state.page - 1});
    }else{
      this.setState({page: this.state.maxPages});
    }
    if(productStore.getActiveEdit()){
      productActions.clearActiveProduct();
    }
  }
  handleSearchChange(e) {
    productActions.updateSearch(e.target.value);
  }
  handleOptionChange(e) {
    productActions.updateSearchTerms(e.target.value);
  }
  updateMaxPages(array){
    this.setState({maxPages: Math.ceil(array.length / 10) - 1});
  }
  handleProductChange(e){
    this.setState({activeProvider: e.target.value});
  }
  render() {
    return(
      <div id='productoverview' className="panel-group">
        <div className="panel panel-default" id="list">
          <div className="panel-head">
            <div className="row" style={{width: 'auto', float: 'right', marginTop: '2%'}}>
              <div className="col-md-4">
                <button className='btn btn-default' onClick={() =>
                  {this.pageBackwards();}}>
                  <span className='glyphicon glyphicon-chevron-left'></span>
                </button>
              </div>
              <div style={{width: 'auto'}} className="col-md-4">
                <p>
                  {this.state.page + 1} / {this.state.maxPages + 1}
                </p>
              </div>
              <div className="col-md-4">
                <button className='btn btn-default' onClick={() =>
                  {this.pageForwards();}}>
                  <span className='glyphicon glyphicon-chevron-right'></span>
                </button>
              </div>
            </div>
            <div id='searchpanel' className="panel-group">
              <div className="panel-body">
                <input id='searchbar' type="text" className="form-control"
                       placeholder="Søk" onChange={this.handleSearchChange}/>
                <select style={{width: 'auto'}} className="form-control" id="searchbar"
                        onChange={this.handleOptionChange.bind(this)}>
                  <option>Navn</option>
                  <option>Produktnr</option>
                </select>
              </div>
            </div>
          </div>
          <div className="panel-body">
            <div className="table" style={{width: '100%'}}>
              <a style={{color: 'black', textDecoration: 'none', fontWeight: 'bold'}} className="th">
                <span className="td">
                  <select style={{width: 'auto'}} className="form-control"
                          onChange={this.handleProductChange.bind(this)}>
                    <option>Leverandør</option>
                  </select>
                </span>
                <span className="td">
                </span>
                <span className="td">
                  Navn
                </span>
                <span className="td">
                  Produktnr
                </span>
                <span className="td">
                  Antall
                </span>
              </a>
              {this.returnPage(this.searchProducts(this.state.products)).map(function(object, i){
                return (
                  <ProductFormRow product={object} />
                );
              })}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
class ProductFormRow extends Component {
  constructor(){
    super();
    this.state = {
      style: {
        backgroundColor:'',
        color: 'black',
        textDecoration: 'none'
      },
      active: false
    };
    this.listeners = {
      productChangeEdit: null
    };
  }
  componentDidMount(){
    this.listeners.productChangeEdit = this.updateActive.bind(this);
    productStore.on("ChangeEdit", this.listeners.productChangeEdit);
  }
  componentWillUnmount() {
    productStore.removeListener("ChangeEdit", this.listeners.productChangeEdit);
  }
  handleDelete(){
    productActions.deleteProduct(this.props.product.id);
  }
  updateActive(){
    if (this.props.product.id != productStore.getActiveEdit()){
      this.setState({
        style: {
          backgroundColor:'',
          color: 'black',
          textDecoration: 'none'
        },
        active: false
      });
    }else{
        this.setState({
          style: {
            backgroundColor:'#4d606f88',
            color: 'black',
            textDecoration: 'none'
          },
          active: true
        });
      }
  }
  rowClick(){
    if (this.state.style.backgroundColor){
      this.setState({
        style: {
          backgroundColor:'',
          color: 'black',
          textDecoration: 'none'
        }
      });
    }else{
      this.setState({
        style: {
          backgroundColor:'#4d606f88',
          color: 'black',
          textDecoration: 'none'
        }
      });
    }
    if(!this.state.active){
      productActions.editProduct(this.props.product.id);
      this.setState({active: true});
    }else{
      productActions.clearActiveProduct();
      this.setState({active: false});
    }
  }
  render() {
    return(
      <a onClick={() => {this.rowClick();}} href='#' className="tr" style={this.state.style}>
        <span className="td">
          <button type='button'
                  style={{float: 'middle', marginLeft: '2%'}}
                  onClick={() =>
                           {
                             this.handleDelete();
                           }}
                  className='btn btn-danger'>
            <span className='glyphicon glyphicon-remove' />
          </button>
        </span>
        <span className="td">
          <img style={{
                 padding: '3%',
                 marginLeft: '2%',
                 float: 'center',
                 width: '30%'
               }}
               src={require('../../images/defaultimage.svg')}
               />
        </span>
        <span className="td">
          {this.props.product.name}
        </span>
        <span className="td">
          {this.props.product.prodnr}
        </span>
        <span className="td">
          {this.props.product.amount}
        </span>
      </a>
    );
  }
}

export default ProductOverview;
