import React, { Component } from 'react';
import ProductForm from './ProductForm.js';
import ProductOverview from './ProductOverview.js';

class Produkter extends Component {
  render() {
    return(
      <div className="Content">
        <div id='background'>
          <ProductForm />
          <ProductOverview />
        </div>
      </div>
    );
  }
}
export default Produkter;
