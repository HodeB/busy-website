import React, { Component } from 'react';
import companyStore from '../../stores/companyStore.js';
import * as companyActions from '../../actions/companyActions.js';

class CompanyForm extends Component {
  constructor(){
    super();
    this.state = {
      firma: '',
      activeEdit: companyStore.getActiveEdit(),
      navn: '',
      orgnr: '',
      adr: '',
      levadr: '',
      postnr: '',
      poststed: '',
      type: '',
      editing: 'false',
      orgnrClassName: 'input-group'
    };
    this.listeners = {
      companyChangeEdit: null,
      companyChangeActive: null
    };
  }
  componentDidMount(){
    this.listeners.companyChangeEdit = this.getActiveEdit.bind(this);
    this.listeners.companyChangeActive = this.getCompany.bind(this);
    companyStore.on("ChangeEdit", this.listeners.companyChangeEdit);
    companyStore.on("ChangeActive", this.listeners.companyChangeActive);
  }
  componentWillUnmount() {
    companyStore.removeListener("ChangeEdit", this.listeners.companyChangeEdit);
    companyStore.removeListener("ChangeActive", this.listeners.companyChangeActive);
  }
  addCompany(data){
    companyActions.addCompany(data);
  }
  updateCompany(id, data){
    companyActions.updateCompany(id, data);
  }
  verifyData(data){
    // Makes sure all required fields are filled out
    // levadr is intentionally left out as it is not a required field.
    if(data.navn && data.orgnr && data.adr && 
       data.postnr && data.poststed && data.type){
      return true;
    }
    return false;
  }
  handleSubmit(e) {
    e.preventDefault();
    const formData = {};
    for (const field in this.refs) {
      formData[field] = this.refs[field].value;
    }
    if(this.verifyData(formData)){
      if (this.state.editing == 'false'){
        this.addCompany(formData);
      }else {
        this.updateCompany(this.state.activeEdit, formData);
        companyActions.clearActiveCompany();
      }
      this.clearActive();
    }else{
      alert('Vennligst fyll ut alle feltene');
    }
  }
  getActiveEdit(){
    var activeEdit = companyStore.getActiveEdit();
    if (activeEdit){
      this.setState({activeEdit: companyStore.getActiveEdit()}, function () {
        companyActions.getCompanyById(this.state.activeEdit);
      });
    }else{
      this.clearActive();
    }
  }
  getCompany(){
    this.setState({firma: companyStore.getActiveCompany()[0]}, function () {
      this.setState({
        navn: this.state.firma.navn,
        orgnr: this.state.firma.orgnr,
        adr: this.state.firma.adr,
        levadr: this.state.firma.levadr,
        postnr: this.state.firma.postnr,
        poststed: this.state.firma.poststed,
        type: this.state.firma.type,
        editing: 'true'});
    });
  }
  clearActive(){
    this.setState({
      navn: '',
      orgnr: '',
      adr: '',
      levadr: '',
      postnr: '',
      poststed: '',
      type: '',
      editing: 'false'
    });
  }
  testNumber(nr){
    var regexnr = new RegExp('^[0-9]*$');
    if(regexnr.test(nr)){
      return true;
    }
    return false;
  }
  navnHandleChange(e){
    var navn = e.target.value;
    navn = navn.charAt(0).toUpperCase() + navn.slice(1);
    this.setState({navn: navn});
  }
  orgnrHandleChange(e){
    if(!this.testNumber(e.target.value)){
      this.setState({
        orgnrClassName: 'input-group has-error'
      });
      if(this.state.orgnrTimer){
        clearTimeout(this.state.orgnrTimer);
      }
      var timer = setTimeout(() => {this.setState({orgnrClassName: 'input-group'});}, 750);
      this.setState({orgnrTimer: timer});
    }else{
      this.setState({
        orgnrClassName: 'input-group',
        orgnr: e.target.value
      });
    }
  }
  adrHandleChange(e){
    var adr = e.target.value;
    adr = adr.charAt(0).toUpperCase() + adr.slice(1);
    this.setState({adr: adr});
  }
  levadrHandleChange(e){
    var levadr = e.target.value;
    levadr = levadr.charAt(0).toUpperCase() + levadr.slice(1);
    this.setState({levadr: levadr});
  }
  postnrHandleChange(e){
    if(!this.testNumber(e.target.value)){
      this.setState({
        postnrClassName: 'input-group has-error'
      });
      if(this.state.postnrTimer){
        clearTimeout(this.state.postnrTimer);
      }
      var timer = setTimeout(() => {this.setState({postnrClassName: 'input-group'});}, 750);
      this.setState({postnrTimer: timer});
    }else{
      this.setState({
        postnrClassName: 'input-group',
        postnr: e.target.value
      });
    }
  }
  poststedHandleChange(e){
    this.setState({poststed: e.target.value.toUpperCase()});
  }
  typeHandleChange(e){
    var type = e.target.value;
    type = type.charAt(0).toUpperCase() + type.slice(1);
    this.setState({type: type});
  }
  render() {
    return (
      <div>
        <form className="input-group"
              onSubmit={this.handleSubmit.bind(this)}>
          <table className="table table-striped">
            <td>
              <input type="text" className="form-control"
                     placeholder="Navn"
                     value={this.state.navn}
                     onChange={this.navnHandleChange.bind(this)}
                     ref="navn"/>
            </td>
            <td>
              <div className={this.state.orgnrClassName}>
                <input type="text" className="form-control"
                       placeholder="Org nummer"
                       value={this.state.orgnr}
                       onChange={this.orgnrHandleChange.bind(this)}
                       ref="orgnr"/>
              </div>
            </td>
            <td>
              <input type="text" className="form-control"
                     placeholder="Adresse"
                     value={this.state.adr}
                     onChange={this.adrHandleChange.bind(this)}
                     ref="adr"/>
            </td>
            <td>
              <input type="text" className="form-control"
                     placeholder="Lev adresse"
                     value={this.state.levadr}
                     onChange={this.levadrHandleChange.bind(this)}
                     ref="levadr"/>
            </td>
            <td>
              <div className={this.state.postnrClassName}>
                <input type="text" className="form-control"
                       placeholder="Post nummer"
                       value={this.state.postnr}
                       onChange={this.postnrHandleChange.bind(this)}
                       ref="postnr"/>
              </div>
            </td>
            <td>
              <input type="text" className="form-control"
                     placeholder="Poststed"
                     value={this.state.poststed}
                     onChange={this.poststedHandleChange.bind(this)}
                     ref="poststed"/>
            </td>
            <td>
              <input type="text" className="form-control"
                     placeholder="Type"
                     value={this.state.type}
                     onChange={this.typeHandleChange.bind(this)}
                     ref="type"/>
            </td>
            <td>
              <button type='submit' className="btn btn-success" >
                <span className='glyphicon glyphicon-plus'></span>
              </button>
            </td>
          </table>
        </form>
      </div>
    );
  };
}

export default CompanyForm;
