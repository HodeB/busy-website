import React, { Component } from 'react';
import PeopleList from './PeopleList.js';
import CompanyList from './CompanyList.js';
import * as peopleActions from '../../actions/peopleActions.js';
import * as companyActions from '../../actions/companyActions.js';

class Kontakter extends Component {
  // Primary component for the contacts module
  constructor(){
    super();
    this.state = {
      page: 'firma'
    };
  }
  ChangePage(page){
    this.setState({page: page});
  }
  render() {
    return(
      <div className="Content">
        <div id='background'>
          <div>
            <ul className="nav nav-tabs">
              <li role="presentation">
                <a href="#" onClick={() =>
                    {
                      this.ChangePage('firma');
                    }}> Firma </ a>
              </li>
              <li role="presentation">
                <a href="#" onClick={() =>
                     {
                       this.ChangePage('personer');
                     }}> Personer </ a>
              </li>
            </ul>
            <PageHandler page={this.state.page}/>
          </div>
        </div>
      </div>
    );
  }
}

class PageHandler extends Component {
  render() {
    switch (this.props.page){
    case 'firma':{
      return(
        <ListCompanies />
      );
    }
    case 'personer':{
      return(
        <ListPeople />
      );
    }
    default:{
      console.log('ERROR: pagehandler received invalid value');
      return(
        <p>
          error
        </p>
      );
    }
    }}
}


class ListCompanies extends Component {
  handleSearchChange(e) {
    companyActions.updateSearch(e.target.value);
  }
  handleOptionChange(e) {
    companyActions.updateSearchTerms(e.target.value);
  }
  render() {
    return(
      <div className="panel-group">
        <div className="panel panel-default" id="list">
          <div className="panel-heading">
            <h4 className="panel-title">
              Firma
            </h4>
          </div>
          <div className="panel-body">
            <div id='searchpanel' className="panel-group">
              <div className="panel-body">
                  <input id='searchbar' type="text" className="form-control"
                         placeholder="Søk" onChange={this.handleSearchChange}/>
                  <select className="form-control" id="searchbar"
                          onChange={this.handleOptionChange.bind(this)}>
                    <option>Navn</option>
                    <option>Org nr</option>
                    <option>Adresse</option>
                    <option>Post nr</option>
                    <option>Post sted</option>
                    <option>Type</option>
                  </select>
              </div>
            </div>
            <CompanyList />
          </div>
        </div>
      </div>
    );
  }
}

class ListPeople extends Component {
  handleSearchChange(e) {
    peopleActions.updateSearch(e.target.value);
  }
  handleOptionChange(e) {
    peopleActions.updateSearchTerms(e.target.value);
  }
  render() {
    return(
      <div className="panel-group">
        <div className="panel panel-default" id="list">
          <div className="panel-heading">
            <h4 className="panel-title">
              Personer
            </h4>
          </div>
          <div>
          <div className="panel-body">
            <div id='searchpanel' className="panel-group">
              <div className="panel-body">
                  <input id='searchbar' type="text" className="form-control"
                         placeholder="Søk" onChange={this.handleSearchChange}/>
                  <select className="form-control" id="searchbar"
                          onChange={this.handleOptionChange.bind(this)}>
                    <option>Navn</option>
                    <option>Firma</option>
                  </select>
              </div>
            </div>
              <PeopleList />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Kontakter;
