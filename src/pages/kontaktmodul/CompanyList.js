import React, { Component } from 'react';
import companyStore from '../../stores/companyStore.js';
import CompanyForm from './CompanyForm.js';
import * as companyActions from '../../actions/companyActions.js';

class CompanyList extends Component {
  constructor(){
    super();
    this.state = {
      firmaer: companyStore.getAll(),
      search: companyStore.getSearch(),
      searchterms: 'Navn',
      page: 0,
      maxPages: 0
    };
    this.listeners = {
      companyChange: null,
      companyChangeSearch: null
    };
  }
  componentDidMount(){
    companyActions.reloadCompanies();
    this.listeners.companyChange = this.getCompanies.bind(this);
    this.listeners.companyChangeSearch = this.getSearch.bind(this);
    companyStore.on("Change", this.listeners.companyChange);
    companyStore.on("ChangeSearch", this.listeners.companyChangeSearch);
  }
  componentWillUnmount() {
    companyStore.removeListener("Change", this.listeners.companyChange);
    companyStore.removeListener("ChangeSearch", this.listeners.companyChangeSearch);
  }
  getCompanies(){
    this.setState({
      firmaer: companyStore.getAll()
    }, function () {
      this.updateMaxPages(this.state.firmaer);
    });
  }
  reloadCompanies(){
    companyActions.reloadCompanies();
  }
  getSearch(){
    this.setState({
      search: companyStore.getSearch(),
      searchterms: companyStore.getSearchTerms(),
      page: 0
    }, function () {
      companyActions.clearActiveCompany();
      this.updateMaxPages(this.searchCompanies(this.state.firmaer));
    });
  }
  returnIndexes(array, startIndex, endIndex){
    var newArray = [];
    var i = startIndex;
    while(i < array.length){
      newArray.push(array[i]);
      i += 1;
      if(i > endIndex){
        break;
      }
    }
    return newArray;
  }
  returnPage(data){
    var page = this.state.page;
    return this.returnIndexes(data, page*10, (page+1)*10);
  }
  pageForwards(){
    if(this.state.page !== this.state.maxPages){
      this.setState({page: this.state.page + 1});
    }else{
      this.setState({page: 0});
    }
    if(companyStore.getActiveEdit()){
      companyActions.clearActiveCompany();
    }
  }
  pageBackwards(){
    if(this.state.page > 0){
      this.setState({page: this.state.page - 1});
    }else{
      this.setState({page: this.state.maxPages});
    }
    if(companyStore.getActiveEdit()){
      companyActions.clearActiveCompany();
    }
  }
  updateMaxPages(array){
    this.setState({maxPages: Math.ceil(array.length / 10) - 1});
  }
  searchCompanies(data){
    var newData = [];
    var search = this.state.search;
    var searchterms = this.state.searchterms;
    if (!search){
      search = '';
    }
    data.map(function(object, i){
      var regex = '[\s\S]*' + search + '[\s\S]*';
      var regex = new RegExp(regex, 'i');
      if(searchterms == 'Navn'){
        if (regex.test(object.navn)){
          newData.push(object);
        }
      }
      else if(searchterms == 'Org nr'){
        if (regex.exec(object.orgnr)){
          newData.push(object);
        }
      }
      else if(searchterms == 'Adresse'){
        if (regex.exec(object.adr)){
          newData.push(object);
        }
      }
      else if(searchterms == 'Post nr'){
        if (regex.exec(object.postnr)){
          newData.push(object);
        }
      }
      else if(searchterms == 'Post sted'){
        if (regex.exec(object.poststed)){
          newData.push(object);
        }
      }
      else if(searchterms == 'Type'){
        if (regex.exec(object.type)){
          newData.push(object);
        }
      }
    });
    return newData;
  }
  render() {
    return (
      <div>
        <center>
          <CompanyForm />
        </center>
        <div id='tablepage'>
          <button id='tablepageleft' className='btn btn-default' onClick={() =>
            {this.pageBackwards();}}>
            <span className='glyphicon glyphicon-chevron-left'></span>
          </button>
          {this.state.page + 1} / {this.state.maxPages + 1}
          <button id='tablepageright' className='btn btn-default' onClick={() =>
                  {this.pageForwards();}}>
            <span className='glyphicon glyphicon-chevron-right'></span>
          </button>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Navn</th>
              <th>Org nummer</th>
              <th>Adresse</th>
              <th>Leverings adresse</th>
              <th>Postnummer</th>
              <th>Poststed</th>
              <th>Type</th>
              <th></th>
              <th>
                <button id='refresh' className='btn btn-info' onClick={this.reloadCompanies}>
                  <span id='listbutton' className='glyphicon glyphicon-refresh'></span>
                </button>
              </th>
            </tr>
          </thead>
          <tbody>
          {this.returnPage(this.searchCompanies(this.state.firmaer)).map(function(object, i){
            return (
              <tr>
                <td>{object.navn}</td>
                <td>{object.orgnr}</td>
                <td>{object.adr}</td>
                <td>{object.levadr}</td>
                <td>{object.postnr}</td>
                <td>{object.poststed}</td>
                <td>{object.type}</td>
                <td>
                  <DeleteButton id={object.id} />
                </td>
                <td>
                  <EditButton id={object.id} />
                </td>
                <td>
                </td>
              </tr>
            );
          })}
          </tbody>
        </table>
      </div>
  );
  }
}

class EditButton extends Component {
  constructor(){
    super();
    this.state = {
      active: 'false',
      buttonclass: 'btn btn-warning'
    };
    this.listeners = {
      peopleChangeEdit: null
    };
  }
  componentDidMount(){
    this.listeners.peopleChangeEdit = this.updateActive.bind(this);
    companyStore.on("ChangeEdit", this.listeners.peopleChangeEdit);
  }
  componentWillUnmount() {
    companyStore.removeListener("ChangeEdit", this.listeners.peopleChangeEdit);
  }
  updateActive(){
    if (this.props.id != companyStore.getActiveEdit()){
      this.setState({active: 'false', buttonclass: 'btn btn-warning'});
    }else{
      this.setState({active: 'true', buttonclass: 'btn btn-danger'});
    }
  }
  handleClick(){
    if(this.state.active == 'false'){
      companyActions.editCompany(this.props.id);
      this.setState({active: 'true'});
    }else{
      companyActions.clearActiveCompany();
      this.setState({active: 'false'});
    }
  }
  render() {
    return(
      <div>
        <button onClick={() =>
                         {
                           this.handleClick();
                         }} className={this.state.buttonclass}>
                         <span id='listbutton' className='glyphicon glyphicon-pencil'></span>
        </button>
      </div>
    );
  }
}

class DeleteButton extends Component {
  constructor(){
    super();
    this.state = {
      active: 'false'
    };
    this.listeners = {
      peopleChangeEdit: null
    };
  }
  componentDidMount(){
    this.listeners.peopleChangeEdit = this.updateActive.bind(this);
    companyStore.on("ChangeEdit", this.listeners.peopleChangeEdit);
  }
  componentWillUnmount() {
    companyStore.removeListener("ChangeEdit", this.listeners.peopleChangeEdit);
  }
  updateActive(){
    if (this.props.id != companyStore.getActiveEdit()){
      this.setState({active: 'false'});
    }else{
      this.setState({active: 'true'});
    }
  }
  render() {
    if(this.state.active == 'false'){
      return(
        <div>
          <button onClick={() =>
                           {
                             companyActions.deleteCompany(this.props.id);
                           }} className='btn btn-danger'>
                           <span id='listbutton' className='glyphicon glyphicon-remove'></span>
          </button>
        </div>
      );
    }else{
      return(<div></div>);
    }
    }
}

export default CompanyList;
