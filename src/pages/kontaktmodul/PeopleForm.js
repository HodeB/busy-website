import React, { Component } from 'react';
import * as peopleActions from '../../actions/peopleActions.js';
import peopleStore from '../../stores/peopleStore.js';
import companyStore from '../../stores/companyStore.js';
import * as companyActions from '../../actions/companyActions.js';

class PeopleForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firmaer: companyStore.getAll(),
      person: {},
      activeEdit: peopleStore.getActiveEdit(),
      fornavn: '',
      etternavn: '',
      tittel: '',
      epost: '',
      tlfnr: '',
      firma: '',
      editing: false,
      epostClassName: 'input-group',
      epostError: false,
      tlfnrClassName: 'input-group'
    };
    this.listeners = {
      peopleChangeEdit: null,
      peopleChangeActive: null,
      companyChange: null
    };
  }
  componentDidMount(){
    this.listeners.peopleChangeEdit = this.getActiveEdit.bind(this);
    this.listeners.peopleChangeActive = this.getPerson.bind(this);
    this.listeners.companyChange = this.getCompanies.bind(this);
    peopleStore.on("ChangeEdit", this.listeners.peopleChangeEdit);
    peopleStore.on("ChangeActive", this.listeners.peopleChangeActive);
    companyStore.on("Change", this.listeners.companyChange);
    this.reloadCompanies();
  }
  componentWillUnmount() {
    peopleStore.removeListener("ChangeEdit", this.listeners.peopleChangeEdit);
    peopleStore.removeListener("ChangeActive", this.listeners.peopleChangeActive);
    companyStore.removeListener("Change", this.listeners.companyChange);
  }
  reloadCompanies(){
    companyActions.reloadCompanies();
  }
  getCompanies(){
    this.setState({
      firmaer: companyStore.getAll()
    });
  }
  addPerson(data){
    peopleActions.addPerson(data);
    peopleActions.reloadPeople();
  }
  verifyData(data){
    if(data.fornavn && data.etternavn && data.tittel && 
       data.epost && data.tlfnr && data.firma){
      return true;
    }
    return false;
  }
  handleSubmit(e) {
    e.preventDefault();
    if(!this.state.epostError){
        const formData = {};
        for (const field in this.refs) {
          formData[field] = this.refs[field].value;
        }
        if(this.verifyData(formData)){
          if (!this.state.editing){
            this.addPerson(formData);
          }else {
            this.updatePerson(this.state.activeEdit, formData);
            peopleActions.clearActivePerson();
          }
          this.clearActive();
        }else{
          alert('Vennligst fyll ut alle felt');
        }
    }else{
      alert('Vennligst fiks alle problemer med feltene');
    }
  }
  updatePerson(id, data){
    peopleActions.updatePerson(id, data);
  }
  getActiveEdit(){
    if (peopleStore.getActiveEdit()){
      this.setState({activeEdit: peopleStore.getActiveEdit()}, function () {
        peopleActions.getPersonById(this.state.activeEdit);
      });
    }else{
      this.clearActive();
    }
  }
  getPerson(){
    this.clearActive();
    this.setState({person: peopleStore.getActivePerson()[0]}, function () {
      this.setState({
        fornavn: this.state.person.fornavn,
        etternavn: this.state.person.etternavn,
        tittel: this.state.person.tittel,
        epost: this.state.person.epost,
        tlfnr: this.state.person.tlfnr,
        firma: this.state.person.firma,
        editing: true,
        activeEdit: this.state.person.id
      });
    });
  }
  clearActive(){
    this.setState({
      fornavn: '',
      etternavn: '',
      tittel: '',
      epost: '',
      tlfnr: '',
      firma: '',
      editing: false,
      person: {},
      activeEdit: '',
      epostClassName: 'input-group',
      epostError: false,
      tlfnrClassName: 'input-group'
    });
  }
  testEmail(email){
    var regexat = new RegExp('[\S]*@[\S]*');
    var regexdot = new RegExp('[\S]*[\.][\S]*');
    if(email){
      if(!regexat.test(email) || !regexdot.test(email)){
        this.setState({
          epostClassName: 'input-group has-error',
          epostError: true
        });
      }else{
        this.setState({
          epostClassName: 'input-group',
          epostError: false
        });
      }
    }else{
      this.setState({
        epostClassName: 'input-group',
        epostError: false
      });
    }
  }
  testTlfnr(nr){
    var regextlf = new RegExp('^[\+0-9][0-9]*$');
    if(!nr){ // If the field is empty it is not an error
      return true;
    } else if(regextlf.test(nr)){
      return true;
    }
    return false;
  }
  fornavnHandleChange(e){
    var fornavn = e.target.value;
    fornavn = fornavn.charAt(0).toUpperCase() + fornavn.slice(1);
    this.setState({fornavn: fornavn});
  }
  etternavnHandleChange(e){
    var etternavn = e.target.value;
    etternavn = etternavn.charAt(0).toUpperCase() + etternavn.slice(1);
    this.setState({etternavn: etternavn});
  }
  tittelHandleChange(e){
    var tittel = e.target.value;
    tittel = tittel.charAt(0).toUpperCase() + tittel.slice(1);
    this.setState({tittel: tittel});
  }
  epostHandleChange(e){
    this.testEmail(e.target.value);
    this.setState({epost: e.target.value});
  }
  tlfnrHandleChange(e){
    if(!this.testTlfnr(e.target.value)){
      this.setState({
        tlfnrClassName: 'input-group has-error'
      });
      if(this.state.tlfnrTimer){
        clearTimeout(this.state.tlfnrTimer);
      }
      var timer = setTimeout(() => {this.setState({tlfnrClassName: 'input-group'});}, 750);
      this.setState({tlfnrTimer: timer});
    }else{
      this.setState({
        tlfnrClassName: 'input-group',
        tlfnr: e.target.value
      });
    }
  }
  firmaHandleChange(e){
    this.setState({firma: e.target.value});
  }
  render() {
    return (
      <div>
        <form className="input-group"
              onSubmit={this.handleSubmit.bind(this)}>
          <table className="table table-striped">
              <td>
                <input type="text" className="form-control"
                       placeholder="Fornavn"
                       value={this.state.fornavn}
                       onChange={this.fornavnHandleChange.bind(this)}
                       ref='fornavn'/>
              </td>
              <td>
                <input type="text" className="form-control"
                       placeholder="Etternavn"
                       value={this.state.etternavn}
                       onChange={this.etternavnHandleChange.bind(this)}
                       ref="etternavn" />
              </td>
              <td>
                <input type="text" className="form-control"
                       placeholder="Tittel"
                       value={this.state.tittel}
                       onChange={this.tittelHandleChange.bind(this)}
                       ref="tittel" />
              </td>
              <td>
                <div className={this.state.epostClassName}>
                  <input type="text" className="form-control"
                         placeholder="Epost"
                         value={this.state.epost}
                         onChange={this.epostHandleChange.bind(this)}
                         ref="epost" />
                </div>
              </td>
              <td>
                <div className={this.state.tlfnrClassName}>
                  <input type="text" className="form-control"
                         placeholder="Telefon nummer"
                         value={this.state.tlfnr}
                         onChange={this.tlfnrHandleChange.bind(this)}
                         ref="tlfnr" />
                </div>
              </td>
              <td>
                <select className="form-control" id="input"
                        value={this.state.firma}
                        onChange={this.firmaHandleChange.bind(this)}
                        ref="firma">
                  <option>Ingen firma</option>
                  {this.state.firmaer.map(function(object){
                    return (
                      <option>{object.navn}</option>
                    );
                  })}
      </select>
        </td>
        <td>
          <button type='submit' className="btn btn-success" >
          <span className='glyphicon glyphicon-plus'></span>
          </button>
        </td>
        </table>
        </form>
      </div>
  );
  }
}

export default PeopleForm;
