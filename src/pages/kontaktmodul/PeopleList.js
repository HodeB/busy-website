import React, { Component } from 'react';
import peopleStore from '../../stores/peopleStore.js';
import PeopleForm from './PeopleForm.js';
import * as peopleActions from '../../actions/peopleActions.js';

class PeopleList extends Component {
  constructor(){
    super();
    this.state = {
      people: peopleStore.getPeople(),
      search: peopleStore.getSearch(),
      searchterms: 'Navn',
      page: 0,
      maxPages: 0
    };
    this.listeners = {
      peopleChange: null,
      peopleChangeSearch: null
    };
  }
  componentDidMount(){
    peopleActions.reloadPeople();
    this.listeners.peopleChange = this.getPeople.bind(this);
    this.listeners.peopleChangeSearch = this.getSearch.bind(this);
    peopleStore.on("Change", this.listeners.peopleChange);
    peopleStore.on("ChangeSearch", this.listeners.peopleChangeSearch);
  }
  componentWillUnmount() {
    peopleStore.removeListener("Change", this.listeners.peopleChange);
    peopleStore.removeListener("ChangeSearch", this.listeners.peopleChangeSearch);
  }
  getPeople(){
    this.setState({
      people: peopleStore.getPeople()
    }, function () {
      this.updateMaxPages(this.state.people);
    });
  }
  reloadPeople(){
    peopleActions.reloadPeople();
    this.getPeople();
  }
  getSearch(){
    this.setState({
      search: peopleStore.getSearch(),
      searchterms: peopleStore.getSearchTerms(),
      page: 0
    }, function () {
      this.updateMaxPages(this.searchPeople(this.state.people));
      peopleActions.clearActivePerson();
    });
  }
  updateMaxPages(array){
    this.setState({maxPages: Math.ceil(array.length / 10) - 1});
  }
  returnIndexes(array, startIndex, endIndex){
    var newArray = [];
    var i = startIndex;
    while(i < array.length){
      newArray.push(array[i]);
      i += 1;
      if(i > endIndex){
        break;
      }
    }
    return newArray;
  }
  returnPage(data){
    var page = this.state.page;
    return this.returnIndexes(data, page*10, (page+1)*10);
  }
  pageForwards(){
    if(this.state.page !== this.state.maxPages){
      this.setState({page: this.state.page + 1});
    }else{
      this.setState({page: 0});
    }
    if(peopleStore.getActiveEdit()){
      peopleActions.clearActivePerson();
    }
  }
  pageBackwards(){
    if(this.state.page > 0){
      this.setState({page: this.state.page - 1});
    }else{
      this.setState({page: this.state.maxPages});
    }
    if(peopleStore.getActiveEdit()){
      peopleActions.clearActivePerson();
    }
  }
  searchPeople(data){
    var newData = [];
    var search = this.state.search;
    var searchterms = this.state.searchterms;
    if (!search){
      search = '';
    }
    data.map(function(object, i){
      var regex = '[\s\S]*' + search + '[\s\S]*';
      var regex = new RegExp(regex, 'i');
      if(searchterms == 'Navn'){
        if (regex.test(object.fornavn + ' ' + object.etternavn)){
          newData.push(object);
        }
      }
      if(searchterms == 'Firma'){
        if (regex.exec(object.firma)){
          newData.push(object);
        }
      }
    });
    return newData;
  }
  render() {
    return (
      <div>
        <center>
          <PeopleForm />
        </center>
        <div id='tablepage'>
          <button id='tablepageleft' className='btn btn-default' onClick={() =>
            {this.pageBackwards();}}>
            <span className='glyphicon glyphicon-chevron-left'></span>
          </button>
          {this.state.page + 1} / {this.state.maxPages + 1}
          <button id='tablepageright' className='btn btn-default' onClick={() =>
                  {this.pageForwards();}}>
            <span className='glyphicon glyphicon-chevron-right'></span>
          </button>
        </div>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Fornavn</th>
              <th>Etternavn</th>
              <th>Jobb</th>
              <th>Epost</th>
              <th>Telefon</th>
              <th>Firma</th>
              <th>
              </th>
              <th>
                <button id='refresh' className='btn btn-info' onClick={this.reloadPeople.bind(this)}>
                  <span id='listbutton' className='glyphicon glyphicon-refresh'></span>
                </button>
              </th>
            </tr>
          </thead>
          <tbody>
            {this.returnPage(this.searchPeople(this.state.people)).map(function(object, i){
            return (
              <tr>
                <td>{object.fornavn}</td>
                <td>{object.etternavn}</td>
                <td>{object.tittel}</td>
                <td>{object.epost}</td>
                <td>{object.tlfnr}</td>
                <td>{object.firma}</td>
                <td>
                  <DeleteButton id={object.id} />
                </td>
                <td>
                  <EditButton id={object.id} />
                </td>
              </tr>
            );
          })}
          </tbody>
        </table>
      </div>
  );
  }
}

class EditButton extends Component {
  constructor(){
    super();
    this.state = {
      active: 'false',
      buttonclass: 'btn btn-warning'
    };
    this.listeners = {
      peopleChangeEdit: null
    };
  }
  componentDidMount(){
    this.listeners.peopleChangeEdit = this.updateActive.bind(this);
    peopleStore.on("ChangeEdit", this.listeners.peopleChangeEdit);
  }
  componentWillUnmount() {
    peopleStore.removeListener("ChangeEdit", this.listeners.peopleChangeEdit);
  }
  updateActive(){
    if (this.props.id != peopleStore.getActiveEdit()){
      this.setState({active: 'false', buttonclass: 'btn btn-warning'});
    }else{
      this.setState({active: 'true', buttonclass: 'btn btn-danger'});
    }
  }
  handleClick(){
    if(this.state.active == 'false'){
      peopleActions.editPerson(this.props.id);
      this.setState({active: 'true'});
    }else{
      peopleActions.clearActivePerson();
      this.setState({active: 'false'});
    }
  }
  render() {
    return(
      <div>
        <button onClick={() =>
                         {
                           this.handleClick();
                         }} className={this.state.buttonclass}>
                         <span id='listbutton' className='glyphicon glyphicon-pencil'></span>
        </button>
      </div>
    );
  }
}

class DeleteButton extends Component {
  constructor(){
    super();
    this.state = {
      active: 'false'
    };
    this.listeners = {
      peopleChangeEdit: null
    };
  }
  componentDidMount(){
    this.listeners.peopleChangeEdit = this.updateActive.bind(this);
    peopleStore.on("ChangeEdit", this.listeners.peopleChangeEdit);
  }
  componentWillUnmount() {
    peopleStore.removeListener("ChangeEdit", this.listeners.peopleChangeEdit);
  }
  updateActive(){
    if (this.props.id != peopleStore.getActiveEdit()){
      this.setState({active: 'false'});
    }else{
      this.setState({active: 'true'});
    }
  }
  render() {
      if(this.state.active == 'false'){
        return(
          <div>
            <button onClick={() =>
                             {
                               peopleActions.deletePerson(this.props.id);
                             }} className='btn btn-danger'>
                             <span className='glyphicon glyphicon-remove'></span>
            </button>
          </div>
        );
      }else{
        return(<div></div>);
      }
  }
}

export default PeopleList;
